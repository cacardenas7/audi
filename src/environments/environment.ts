// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDl89X_y7wq5DM7Tyqe1ZZC5xlvwaru1iM",
    authDomain: "employees-24621.firebaseapp.com",
    databaseURL: "https://employees-24621.firebaseio.com",
    projectId: "employees-24621",
    storageBucket: "employees-24621.appspot.com",
    messagingSenderId: "1039507270018"
  }
};
