import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/quizz/login/login.component';
import { QuizzComponent } from './components/quizz/quizz.component';
import { AdminComponent } from './components/quizz/admin/admin.component';

const appRoutes: Routes = [
  { path: '', component: QuizzComponent},
  { path: 'login', component: LoginComponent },
  { path: 'private', component: AdminComponent },
  { path: 'logout', component: QuizzComponent },
  { path: '**', component: QuizzComponent}
]

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
