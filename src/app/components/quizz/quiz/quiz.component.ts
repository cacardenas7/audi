import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

//  Service 
import { QuizService } from '../../../services/quiz.service';

// Class
import { QuizzData } from '../../../models/quizz';

// toastr
import { ToastrService } from 'ngx-toastr';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-product',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  companyList: AngularFireList<any>;
  public objArray: any = [];
  selectedOption: string;
  printedOption: string;
  public obj: any = [];
  public data:any;
  constructor(
    public quizService: QuizService,
    private toastr: ToastrService,

    private firebase: AngularFireDatabase

  ) { }

  print() {
    this.printedOption = this.selectedOption;
  }
  ngOnInit() {
    this.quizService.getQuiz();
    this.companyList = this.firebase.list('company');
    let dataCompany = [];
    this.data =this.quizService.getCompany();
    this.resetForm();
  }
  onSubmit(quiz:NgForm) {
    this.quizService.insertProduct(quiz.value);
    this.toastr.success('Acción Realizada', 'Encuesta Registrada');
    this.resetForm(quiz);
    /* else {
      this.quizService.updateProduct(productForm.value);
      this.resetForm(productForm);
      th*¿is.toastr.success('Acción Realizada', 'Empleado Actualizado');
    }*/
  }

  resetForm(productForm?: NgForm) {
    if (productForm != null)
      productForm.reset();
    this.quizService.selectedquiz = new QuizzData();
  }

}
