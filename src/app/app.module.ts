import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

// components
import { QuizzComponent } from './components/quizz/quizz.component';
import { QuizComponent } from './components/quizz/quiz/quiz.component';
import { QuizListComponent } from './components/quizz/quiz-list/quiz-list.component';
import { LoginComponent } from './components/quizz/login/login.component';
import { AdminComponent } from './components/quizz/admin/admin.component';

// service
import { QuizService } from './services/quiz.service';

//rutas
import { appRoutingProviders, routing } from "./app.routing";

// Toastr
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';

@NgModule({
  declarations: [
    AppComponent,
    QuizComponent,
    QuizListComponent,
    QuizzComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    routing,
    
  ],
  providers: [
    QuizService,
    appRoutingProviders,
    AuthService,
    AngularFireAuth
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
