import { Injectable } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import { resolve } from 'url';
import { reject } from 'q';
@Injectable()
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth
  ) { }

  login(email:string, pass:string){
    return new Promise((resolve, reject)=>{
      this.afAuth.auth.signInWithEmailAndPassword(email,pass)
      .then(user =>resolve(user), err=>reject(err));
    })
  }
  register(email:string, pass:string){
    return new Promise((resolve, reject)=>{
      this.afAuth.auth.createUserWithEmailAndPassword(email,pass)
      .then(user =>resolve(user), err=>reject(err));
    })
  }

  getAuth(){
    return this.afAuth.authState.map(auth =>auth);
  }

  logout(){
    return this.afAuth.auth.signOut();
  }
}
