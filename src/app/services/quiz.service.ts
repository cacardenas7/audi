import { Injectable } from '@angular/core';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

// Model
import { QuizzData } from '../models/quizz';
import { Company } from '../models/company';

@Injectable()
export class QuizService {

  quizList: AngularFireList<any>;
  companyList: AngularFireList<any>;
  selectedquiz: QuizzData = new QuizzData();

  constructor(private firebase: AngularFireDatabase) { }

  getQuiz() {
    return this.quizList = this.firebase.list('quiz');
  }
  getCompany() {
    let array=[];
    this.firebase.list('company')
    .snapshotChanges().subscribe(res=>{
      res.map(test =>{
        //console.log(test.payload.val().name)
        array.push({
          id: test.payload.key,
          name: test.payload.val().name
        })
      })
    })
    return array;
    /*let dataCompany = [];
    this.companyList.valueChanges().subscribe(data => {
      return dataCompany = data;*/
    }

  insertProduct(quiz: QuizzData) {
    const quizList = this.firebase.list('quiz');
    console.log(quiz.name)
    console.log(quiz.alcance)
    console.log(quiz.plan)
    console.log(quiz.estrategico)
    console.log(quiz.eccommerce)
    console.log(quiz.motorbd)
    console.log(quiz.red)
    quizList.push({
      name: quiz.name,
      alcance: quiz.alcance,
      plan: quiz.plan,
      estrategico: quiz.estrategico,
      eccommerce: quiz.eccommerce,
      procesos: quiz.procesos,
      motorbd: quiz.motorbd,
      red: quiz.red
    });
  }

  insertCompany(company: Company) {
    const item = this.firebase.list('company');
    item.push({
      name: company.name
    })
  }

  /*updateProduct(quiz: QuizzData) {
    this.quizList.update(quiz.$key, {
      name: quiz.name,
      lastname: quiz.lastname,
      category: quiz.category,
      location: quiz.location,
      phone: quiz.phone,
      salary: quiz.salary
    });
  }*/


  deleteCompany($key: string){
    this.companyList = this.firebase.list('company');
    this.companyList.remove($key)
  }
  deleteProduct($key: string) {
    this.quizList.remove($key);
  }
}
